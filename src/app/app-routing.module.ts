import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainViewComponent} from './components/main-view/main-view.component';
import {AuthGuardService} from './services/auth-guard/auth-guard.service';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'user'},
    // {
    //     path: 'dashboard',
    //     canActivate: [AuthGuardService],
    //     component: MainViewComponent,
    //     loadChildren: './dashboard/dashboard.module#DashboardModule'
    // },
    {
        path: 'company',
        canActivate: [AuthGuardService],
        component: MainViewComponent,
        loadChildren: './company/company.module#CompanyModule'
    },
    {
        path: 'user',
        canActivate: [AuthGuardService],
        component: MainViewComponent,
        loadChildren: './user/user.module#UserModule'
    },
    {
        path: 'category',
        canActivate: [AuthGuardService],
        component: MainViewComponent,
        loadChildren: './category/category.module#CategoryModule'
    },
    {
        path: 'process',
        canActivate: [AuthGuardService],
        component: MainViewComponent,
        loadChildren: './process/process.module#ProcessModule'
    },
    {
        path: 'payment',
        canActivate: [AuthGuardService],
        component: MainViewComponent,
        loadChildren: './payment/payment.module#PaymentModule'
    },
    // {
    //     path: 'help',
    //     canActivate: [AuthGuardService],
    //     component: MainViewComponent,
    //     loadChildren: './help/help.module#HelpModule'
    // },
    {path: 'login', loadChildren: './login/login.module#LoginModule'},
    {path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
