import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';
import {CoreModule} from '../modules/core.module';

@NgModule({
    declarations: [DashboardComponent],
    imports: [
        CommonModule,
        CoreModule.forRoot(),
        DashboardRoutingModule
    ]
})
export class DashboardModule {
}
