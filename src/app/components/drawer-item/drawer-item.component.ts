import {Component, Input, OnInit} from '@angular/core';
import {NavEntity} from '../../entity/NavEntity';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Event, NavigationEnd, Router} from '@angular/router';
import {DrawerService} from '../../services/drawer/drawer.service';

@Component({
    selector: 'mp-drawer-item',
    templateUrl: './drawer-item.component.html',
    styleUrls: ['./drawer-item.component.scss'],
    animations: [
        trigger('indicatorRotate', [
            state('collapsed', style({transform: 'rotate(0deg)'})),
            state('expanded', style({transform: 'rotate(180deg)'})),
            transition('expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ])
    ]
})
export class DrawerItemComponent implements OnInit {

    expanded: boolean;
    @Input() item: NavEntity;
    @Input() depth = 1;

    constructor(public drawerService: DrawerService, public router: Router) {
        if (typeof this.depth === 'undefined') {
            this.depth = 1;
        }
    }

    ngOnInit() {
        this.checkAutoExpanded();
        this.initChildRouteWithParentUrl();
        this.router.events.subscribe(($event: Event) => {
            if ($event instanceof NavigationEnd) {
                this.checkAutoExpanded();
                this.initChildRouteWithParentUrl();
            }
        });
    }

    private isParent(): boolean {
        const parentUrls = this.item.showWhenParents || [];
        if (parentUrls.length === 0) {
            return true;
        }
        for (const url of parentUrls) {
            const is = this.router.url.substring(0, url.length) === url;
            if (is) {
                return true;
            }
        }
        return false;
    }

    private checkAutoExpanded() {
        this.expanded = this.isParent() && this.item.expandToParent;
    }

    private getParentUrl(): string {
        const parentUrls = this.item.showWhenParents || [];
        for (const url of parentUrls) {
            const is = this.router.url.substring(0, url.length) === url;
            if (is) {
                return url;
            }
        }
        return null;
    }

    private initChildRouteWithParentUrl() {
        const parentUrl = this.getParentUrl();

        if (parentUrl != null) {
            for (const nav of this.item.children) {
                if (nav.route) {
                    nav.route = nav.route.replace('{parent}', parentUrl);
                }
            }
        }
    }

    onItemSelected(item: NavEntity) {
        if (this.isParent() && item.children && item.children.length > 0) {
            this.expanded = !this.expanded;
        } else if ((!this.isParent() && item.route) || item.route) {
            if (this.item.externalUrl) {
                window.open(item.route);
            } else {
                this.router.navigate([item.route]);
                if (this.drawerService.isOverOpened()) {
                    this.drawerService.closeDrawer();
                }
            }
        }
    }
}
