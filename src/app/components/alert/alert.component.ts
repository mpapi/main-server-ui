import {Component, Input} from '@angular/core';

@Component({
    selector: 'mp-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent {

    @Input() color = 'primary';

    constructor() {
    }

}
