import {Component, ContentChild, Input, OnInit, TemplateRef, ViewEncapsulation} from '@angular/core';
import {BreadcrumbEntity} from '../../entity/BreadcrumbEntity';

@Component({
    selector: 'mp-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class HeaderComponent implements OnInit {

    @Input() title: string;
    @Input() breadcrumbs: BreadcrumbEntity[];
    @Input() subtitle: string;
    @ContentChild('list') parentList: TemplateRef<any>;

    constructor() {
    }

    ngOnInit() {
    }

}
