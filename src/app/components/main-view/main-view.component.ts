import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DrawerService} from '../../services/drawer/drawer.service';
import {NavEntity} from '../../entity/NavEntity';
import {Router} from '@angular/router';
import {UserEntity} from '../../entity/UserEntity';
import {SessionService} from '../../services/session/session.service';
import {UserService} from '../../services/user/user.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';

@Component({
    selector: 'mp-main-view',
    templateUrl: './main-view.component.html',
    styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit, AfterViewInit {

    @ViewChild('drawer') drawer: ElementRef;
    screenWidth: number;
    user: UserEntity;
    navItems: NavEntity[] = [
        // {
        //     displayName: 'Dashboard',
        //     iconName: 'dashboard',
        //     divider: {after: true},
        //     route: '/dashboard'
        // },
        {
            displayName: 'Şirketler',
            iconName: 'business',
            route: '/company/list'
        },
        {
            displayName: 'Kullanıcılar',
            iconName: 'people',
            route: '/user/list',
            expandToParent: true,
            showWhenParents: ['/user/edit'],
            children: [
                {
                    displayName: 'Genel Bilgiler',
                    route: '{parent}/information',
                    iconName: 'info'
                },
                {
                    displayName: 'Detaylar',
                    route: '{parent}/details',
                    iconName: 'details'
                },
                {
                    displayName: 'Default Parametreler',
                    route: '{parent}/defaults',
                    iconName: 'flag1'
                },
                {
                    displayName: 'İzinler',
                    route: '{parent}/permission',
                    iconName: 'perm_device_information'
                },
                {
                    displayName: 'Görevler',
                    route: '{parent}/tasks',
                    iconName: 'query_builder'
                },
                {
                    displayName: 'Şirket Yönetimi',
                    route: '{parent}/company-management',
                    iconName: 'business'
                },
                {
                    displayName: 'Veritabanı Yönetimi',
                    route: '{parent}/database-management',
                    iconName: 'apps'
                }
            ]
        },
        {
            displayName: 'Kategoriler',
            iconName: 'dvr',
            route: '/category/users'
        },
        {
            displayName: 'İşlemler',
            iconName: 'perm_data_setting',
            route: '/process/users',
            expandToParent: true,
            divider: {after: true},
            showWhenParents: [
                '/process/list',
                '/process/mp-dashboard',
                '/process/add',
                '/process/edit',
                '/process/add/mp-analysis',
                '/process/edit/mp-analysis'
            ],
            children: [
                {
                    displayName: 'İşlem Listesi',
                    route: '/process/list',
                    iconName: 'info',
                    divider: {after: true},
                    expandToParent: true,
                    showWhenParents: [
                        '/process/edit',
                        '/process/edit/mp-analysis'
                    ],
                    children: [
                        {
                            displayName: 'Genel Bilgiler',
                            route: '{parent}/information',
                            iconName: 'info'
                        },
                        {
                            displayName: 'Sorgular',
                            route: '{parent}/queries',
                            iconName: 'query_builder',
                        },
                        {
                            displayName: 'Hızlı Erişim Kuralları',
                            route: '{parent}/present-params',
                            iconName: 'present_to_all',
                            divider: {after: true}
                        },
                        {
                            displayName: 'MP-Analysis',
                            route: '{parent}/mp-analysis',
                            iconName: 'apps',
                            expandToParent: true,
                            showWhenParents: [
                                '/process/add/mp-analysis',
                                '/process/edit/mp-analysis'
                            ],
                            children: [
                                {
                                    displayName: 'Grafikler',
                                    route: '{parent}/charts',
                                    iconName: 'bubble_chart',
                                    divider: {after: true}
                                }
                            ]
                        }
                    ]
                },
                {
                    displayName: 'MP-Dashboard',
                    route: '/process/mp-dashboard',
                    iconName: 'touch_app'
                }
            ]
        },
        // {
        //     displayName: 'Ödeme İşlemleri',
        //     iconName: 'payment',
        //     route: '/payment'
        // },
        {
            displayName: 'Kullanım Klavuzu',
            iconName: 'description',
            route: '/user-guide.html',
            externalUrl: true
        },
        {
            displayName: 'Server API-Doc',
            iconName: 'description',
            route: '/docs',
            externalUrl: true
        },
        {
            displayName: 'Client API-Doc',
            iconName: 'description',
            route: 'http://client-server.hakanketen.com/docs',
            externalUrl: true
        },
        // {
        //     displayName: 'Destek',
        //     iconName: 'help',
        //     route: '/help'
        // }
    ];

    constructor(private drawerService: DrawerService,
                private session: SessionService,
                private userService: UserService,
                private snackBar: SnackBarService,
                private router: Router) {
        this.user = session.getUser();
        this.screenWidth = window.innerWidth;
        window.onresize = () => {
            this.screenWidth = window.innerWidth;
        };
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.drawerService.drawer = this.drawer;
    }

    logout() {
        this.userService.logout().then(result => {
            this.snackBar.openAutoHide('Çıkış Başarılı');
            this.gotoLogin();
        }, error => {
            if (error.error === 'TOKEN_NOT_FOUND') {
                this.gotoLogin();
            }
        });
    }

    private gotoLogin() {
        this.session.clear();
        this.router.navigateByUrl('/login');
    }
}
