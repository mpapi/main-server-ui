import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CategoryRoutingModule} from './category-routing.module';
import {ListComponent} from './list/list.component';
import {UsersComponent} from './users/users.component';
import {CoreModule} from '../modules/core.module';
import {CategoryItemComponent} from './category-item/category-item.component';
import {AddDialogComponent} from './add-dialog/add-dialog.component';

@NgModule({
    declarations: [
        ListComponent,
        UsersComponent,
        CategoryItemComponent,
        AddDialogComponent,
    ],
    imports: [
        CommonModule,
        CoreModule.forRoot(),
        CategoryRoutingModule
    ],
    entryComponents: [
        CategoryItemComponent,
        AddDialogComponent,
    ]
})
export class CategoryModule {
}
