import {Component, OnInit} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {CategoryService} from '../../services/category/category.service';
import {DataService} from '../../services/data/data.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {CategoryEntity} from '../../entity/CategoryEntity';
import {MatDialog} from '@angular/material';
import {AddDialogComponent} from '../add-dialog/add-dialog.component';

@Component({
    selector: 'mp-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    user: UserEntity;
    categories: CategoryEntity[];

    constructor(private categoryService: CategoryService,
                private data: DataService,
                private snackBar: SnackBarService,
                public dialog: MatDialog) {
        this.user = data.getSelectUser();
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.categoryService.list(this.user._id, true).then(result => {
            this.categories = result['items'];
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    showAddDialog() {
        const dialog = this.dialog.open(AddDialogComponent, {
            width: '500px',
            data: null
        });
        dialog.afterClosed().subscribe(item => {
            if (item) {

                this.categories.push(item);

                this.load();
            }
        });
    }
}
