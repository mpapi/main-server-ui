import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {CategoryEntity} from '../../entity/CategoryEntity';
import {CategoryService} from '../../services/category/category.service';
import {DataService} from '../../services/data/data.service';

@Component({
    selector: 'mp-add-dialog',
    templateUrl: './add-dialog.component.html',
    styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {

    category: CategoryEntity;
    loading = false;

    constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public parentId: string,
                private categoryService: CategoryService,
                private data: DataService,
                private snackBar: SnackBarService) {
        const user = data.getSelectUser();
        this.category = new CategoryEntity();
        this.category.parentId = parentId;
        this.category.userId = user._id;
    }

    ngOnInit() {
    }

    save() {
        if (this.loading) {
            return;
        }

        this.loading = true;
        this.categoryService.add(this.category).then(result => {
            this.snackBar.openAutoHide('Kategori Eklendi.');
            this.dialogRef.close(this.category);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        }).then(() => this.loading = false);
    }
}
