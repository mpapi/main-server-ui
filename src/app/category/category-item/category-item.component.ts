import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {CategoryEntity} from '../../entity/CategoryEntity';
import {MatDialog} from '@angular/material';
import {AddDialogComponent} from '../add-dialog/add-dialog.component';
import {CategoryService} from '../../services/category/category.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';

@Component({
    selector: 'mp-category-item',
    templateUrl: './category-item.component.html',
    styleUrls: ['./category-item.component.scss'],
    animations: [
        trigger('indicatorRotate', [
            state('collapsed', style({transform: 'rotate(0deg)'})),
            state('expanded', style({transform: 'rotate(180deg)'})),
            transition('expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ])
    ]
})
export class CategoryItemComponent implements OnInit {

    @Input() category: CategoryEntity;
    @Input() parentId: string = null;
    @Input() depth = 1;
    @Output() refresh = new EventEmitter<void>();
    expanded: boolean;
    hasChild: boolean;

    editing: boolean;
    loading = false;

    constructor(private categoryService: CategoryService,
                private snackBar: SnackBarService,
                public dialog: MatDialog) {
        if (typeof this.depth === 'undefined') {
            this.depth = 1;
        }
    }

    ngOnInit() {
        this.hasChild = this.category.children && this.category.children.length > 0;
        this.editing = !this.category._id;
    }

    onToggleExpanded() {
        if (this.hasChild) {
            this.expanded = !this.expanded;
        }
    }

    showAddDialog(): void {
        const addDialog = this.dialog.open(AddDialogComponent, {
            width: '500px',
            data: this.category._id
        });
        addDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh.emit();
            }
        });
    }

    save() {
        if (this.loading) {
            return;
        }

        this.loading = true;

        const promise = this.category._id
            ? this.categoryService.edit(this.category)
            : this.categoryService.add(this.category);

        promise.then(() => {
            this.snackBar.openAutoHide('Kategori Kaydedildi.');
            this.refresh.emit();
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        }).then(() => this.loading = false);
    }

    delete() {
        this.snackBar
            .openAutoHide('Tüm alt kategorilerle beraber silinecektir! Silinsin mi?', 'Evet')
            .subscribe(() => {

                try {

                    const result = this.categoryService.delete(this.category._id);

                    this.snackBar.openAutoHide(result['modifiedCount'] + ' Kategori Etkilendi.');
                    this.refresh.emit();

                } catch (err) {

                    this.snackBar.openAutoHide(err.error || err.message);

                }

            });
    }
}
