import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {ListComponent} from './list/list.component';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'list'},
    {path: 'users', component: UsersComponent},
    {path: 'list', component: ListComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoryRoutingModule {
}
