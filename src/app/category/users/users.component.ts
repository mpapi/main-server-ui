import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {UserService} from '../../services/user/user.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import {UserEntity} from '../../entity/UserEntity';

@Component({
    selector: 'mp-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

    displayedColumns: string[] = ['company.title', 'fullname', 'username', 'email', 'active', 'actions'];
    dataSource = new MatTableDataSource<any>([]);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private userService: UserService,
                private snackBar: SnackBarService,
                private data: DataService,
                public dialog: MatDialog,
                private router: Router) {
    }

    ngOnInit() {
        const ds = this.data.get('userList');
        if (ds != null) {
            this.setDataSource(ds);
        }

        this.load();
    }

    ngOnDestroy() {
        this.data.set('userList', this.dataSource.data);
    }

    setDataSource(data: any[]) {
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    load() {
        this.userService.list().then(result => {
            this.setDataSource(result['items']);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    applyFilter(filterText: any) {
        this.dataSource.filter = filterText.trim();
    }

    goto(user: UserEntity) {
        this.data.setSelectUser(user);
        return this.router.navigateByUrl('/category/list');
    }
}
