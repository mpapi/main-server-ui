import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CompanyRoutingModule} from './company-routing.module';
import {ListComponent} from './list/list.component';
import {CoreModule} from '../modules/core.module';
import {CompanyDialogComponent} from './company-dialog/company-dialog.component';

@NgModule({
    declarations: [ListComponent, CompanyDialogComponent],
    imports: [
        CommonModule,
        CompanyRoutingModule,
        CoreModule
    ],
    entryComponents: [
        ListComponent,
        CompanyDialogComponent
    ],
})
export class CompanyModule {
}
