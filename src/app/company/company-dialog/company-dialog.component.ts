import {Component, Inject, OnInit} from '@angular/core';
import {CompanyEntity} from '../../entity/CompanyEntity';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as uuid from 'uuid';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {CompanyService} from '../../services/company/company.service';

@Component({
    selector: 'mp-company-dialog',
    templateUrl: './company-dialog.component.html',
    styleUrls: ['./company-dialog.component.scss']
})
export class CompanyDialogComponent implements OnInit {

    tempCompany: CompanyEntity;
    company: CompanyEntity;

    isEdit: boolean;
    loading = false;

    constructor(public dialogRef: MatDialogRef<CompanyDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: {
                    company: CompanyEntity
                },
                private companyService: CompanyService,
                private snackBar: SnackBarService) {


        this.isEdit = !!data.company;
        this.company = data.company || new CompanyEntity();
        this.tempCompany = {...this.company};
    }

    ngOnInit() {
    }

    generateMachineKey() {
        this.company.machineKey = uuid.v1();
    }

    async save() {
        this.loading = true;

        const company: any = {
            title: this.company.title,
            userLimit: this.company.userLimit,
            expiredTime: this.company.expiredTime,
            machineKey: this.company.machineKey,
            active: this.company.active,
            verified: this.company.verified,
            deleted: this.company.deleted,
        };

        try {
            if (this.isEdit) {
                company._id = this.company._id;
                await this.companyService.edit(company);
            } else {
                await this.companyService.add(company);
            }

            this.snackBar.openAutoHide('Kaydedildi.');

            this.dialogRef.close(true);

        } catch (err) {

            this.snackBar.openAutoHide(err.error || err.message);
        }

        this.loading = false;
    }
}
