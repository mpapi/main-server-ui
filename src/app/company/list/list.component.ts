import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {CompanyService} from '../../services/company/company.service';
import {CompanyEntity} from '../../entity/CompanyEntity';
import {CompanyDialogComponent} from '../company-dialog/company-dialog.component';
import * as _ from 'lodash';
import {SessionService} from '../../services/session/session.service';
import {UserEntity} from '../../entity/UserEntity';

@Component({
    selector: 'mp-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

    user: UserEntity;
    isRootRole = false;

    displayedColumns: string[] = ['title', 'userLimit', 'expiredTime', 'active', 'verified'];
    dataSource = new MatTableDataSource<any>([]);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private sessionService: SessionService,
                private companyService: CompanyService,
                private snackBar: SnackBarService,
                private data: DataService,
                public dialog: MatDialog,
                private router: Router) {

        this.user = sessionService.getUser();
        this.isRootRole = _.indexOf(this.user.roles, 'root') >= 0;

        if (this.isRootRole) {
            this.displayedColumns.push('deleted');
        }

        this.displayedColumns.push('actions');
    }

    ngOnInit() {
        const ds = this.data.get('companyList');
        if (ds != null) {
            this.setDataSource(ds);
        }

        this.load();
    }

    ngOnDestroy(): void {
        this.data.set('companyList', this.dataSource.data);
    }

    setDataSource(data: any[]) {
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        // this.dataSource.filterPredicate = (datas: Element, filter: string) => this.searchPipe.searchItem(data, filter);
    }


    async load() {
        try {
            const result = await this.companyService.list();

            this.setDataSource(result['items']);

        } catch (err) {

            this.snackBar.openAutoHide(err.error || err.message);
        }
    }

    applyFilter(filterText: any) {
        this.dataSource.filter = filterText.trim();
    }

    show(company?: CompanyEntity) {
        const dialogRef = this.dialog.open(CompanyDialogComponent, {
            minWidth: '500px',
            data: {company: _.cloneDeep(company)}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {

                this.load();
            }
        });
    }

    delete(index?: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(async () => {
                const companyId = this.dataSource.data[index]._id;

                try {
                    await this.companyService.delete(companyId);

                    if (!this.isRootRole) {
                        const ds = this.dataSource.data;
                        ds.splice(index, 1);
                        this.dataSource.data = ds;
                    }

                    this.load();
                } catch (err) {
                    this.snackBar.openAutoHide(err.error || err.message);
                }
            });
    }
}
