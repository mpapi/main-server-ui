import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {ListComponent} from './list/list.component';
import {CoreModule} from '../modules/core.module';
import {UserInformationComponent} from './user-information/user-information.component';
import {UserCompanyManagementComponent} from './user-company-management/user-company-management.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {UserDatabaseManagementComponent} from './user-database-management/user-database-management.component';
import {UserTasksComponent} from './user-tasks/user-tasks.component';
import {UserPermissionComponent} from './user-permission/user-permission.component';
import {NewUserDialogComponent} from './new-user-dialog/new-user-dialog.component';
import { UserDefaultsComponent } from './user-defaults/user-defaults.component';

@NgModule({
    declarations: [
        ListComponent,
        UserInformationComponent,
        UserCompanyManagementComponent,
        UserDetailsComponent,
        UserDatabaseManagementComponent,
        UserTasksComponent,
        NewUserDialogComponent,
        UserPermissionComponent,
        UserDefaultsComponent
    ],
    imports: [
        CommonModule,
        CoreModule.forRoot(),
        UserRoutingModule
    ],
    entryComponents: [
        ListComponent,
        NewUserDialogComponent,
        UserTasksComponent
    ],
})
export class UserModule {
}
