import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {UserInformationComponent} from './user-information/user-information.component';
import {UserTasksComponent} from './user-tasks/user-tasks.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {UserDatabaseManagementComponent} from './user-database-management/user-database-management.component';
import {UserCompanyManagementComponent} from './user-company-management/user-company-management.component';
import {UserPermissionComponent} from './user-permission/user-permission.component';
import {UserDefaultsComponent} from './user-defaults/user-defaults.component';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'list'},
    {path: 'list', component: ListComponent},
    {path: 'edit/information', component: UserInformationComponent},
    {path: 'edit/tasks', component: UserTasksComponent},
    {path: 'edit/details', component: UserDetailsComponent},
    {path: 'edit/defaults', component: UserDefaultsComponent},
    {path: 'edit/database-management', component: UserDatabaseManagementComponent},
    {path: 'edit/company-management', component: UserCompanyManagementComponent},
    {path: 'edit/permission', component: UserPermissionComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
