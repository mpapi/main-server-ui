import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {MatDialog} from '@angular/material';
import * as _ from 'lodash';
import {ProcessEntity} from '../../entity/ProcessEntity';
import {UserService} from '../../services/user/user.service';
import {CompanyService} from '../../services/company/company.service';
import {ProcessService} from '../../services/process/process.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import {UserChangesClass} from '../../classes/user-changes.class';
import * as uuid from 'uuid';

@Component({
    selector: 'mp-user-tasks',
    templateUrl: './user-tasks.component.html',
    styleUrls: ['./user-tasks.component.scss']
})
export class UserTasksComponent extends UserChangesClass implements OnInit, OnDestroy {

    @Input() user: UserEntity;
    @Output() userChange: EventEmitter<any> = new EventEmitter<any>();
    @Input() processes: ProcessEntity[];

    taskTypes: any[] = [
        {
            title: 'Uploader',
            value: 'uploader'
        }
    ];

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected dialog: MatDialog,
                protected router: Router) {
        super(userService, companyService, processService, snackBar, data, router);
    }

    ngOnInit(): void {
        super.onInit();
    }

    ngOnDestroy(): void {
        super.onDestroy();
    }

    addTask() {
        if (!_.isArray(this.user.tasks)) {
            this.user.tasks = [];
        }

        const length = (this.user.tasks.length + 1);
        this.user.tasks.push({
            key: uuid.v1().substring(0, 8),
            title: 'Görev ' + length,
            data: [],
            options: {}
        });
    }

    addData(task) {
        if (!_.isArray(task.data)) {
            task.data = [];
        }

        task.data.push({
            parameters: []
        });
    }

    delete(index: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(() => {
                this.user.tasks.splice(index, 1);
            });
    }

    getQueryItems(processKey) {
        const processItem = _.find(this.processes, ['processKey', processKey]);
        return (processItem && processItem.queryItems) || [];
    }

    getParamNames(processKey, queryKey) {
        if (!processKey || !queryKey) {
            return [];
        }

        const processItem = _.find(this.processes, ['processKey', processKey]);
        const queryItem = _.find(processItem && processItem.queryItems, ['queryKey', queryKey]);
        return _.map(queryItem.params, p => p.name);
    }

}
