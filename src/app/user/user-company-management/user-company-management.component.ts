import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import * as _ from 'lodash';
import {UserEntity} from '../../entity/UserEntity';
import {UserChangesClass} from '../../classes/user-changes.class';
import {UserService} from '../../services/user/user.service';
import {CompanyService} from '../../services/company/company.service';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';

@Component({
    selector: 'mp-user-company-management',
    templateUrl: './user-company-management.component.html',
    styleUrls: ['./user-company-management.component.scss']
})
export class UserCompanyManagementComponent extends UserChangesClass  implements OnInit, OnDestroy {

    @Input() user: UserEntity;
    @Output() userChange: EventEmitter<any> = new EventEmitter<any>();

    @Input() set companies(companies) {
        this.dataSource = new MatTableDataSource(companies);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    displayedColumns = ['title', 'actions'];
    dataSource: MatTableDataSource<any> = new MatTableDataSource([]);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected dialog: MatDialog,
                protected router: Router) {
        super(userService, companyService, processService, snackBar, data, router);
    }

    ngOnInit(): void {
        super.onInit();
    }

    ngOnDestroy(): void {
        super.onDestroy();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    setAdminCompany(company) {
        if (!this.user.adminCompanies) {
            this.user.adminCompanies = [];
        }
        this.user.adminCompanies.push(company['_id']);
    }

    isAdminCompany(company): boolean {
        if (!this.user.adminCompanies) {
            return false;
        }
        return this.user.adminCompanies.indexOf(company['_id']) >= 0;
    }

    removeAdminCompany(company) {

        const index: number = _.indexOf(this.user.adminCompanies, company['_id']);

        if (index !== -1) {
            this.user.adminCompanies.splice(index, 1);
        }
    }
}
