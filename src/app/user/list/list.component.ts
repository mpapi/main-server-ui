import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {UserService} from '../../services/user/user.service';
import {DataService} from '../../services/data/data.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {UserEntity} from '../../entity/UserEntity';
import {NewUserDialogComponent} from '../new-user-dialog/new-user-dialog.component';

@Component({
    selector: 'mp-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

    displayedColumns: string[] = ['company.title', 'fullname', 'username', 'email', 'active', 'actions'];
    dataSource = new MatTableDataSource<any>([]);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private userService: UserService,
                private snackBar: SnackBarService,
                private data: DataService,
                public dialog: MatDialog,
                private router: Router) {

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    ngOnInit() {
        const ds = this.data.get('userList');
        if (ds != null) {
            this.setDataSource(ds);
        }

        this.load();
    }

    ngOnDestroy(): void {
        this.data.set('userList', this.dataSource.data);
    }

    setDataSource(data: any[]) {
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    load() {
        this.userService.list().then(result => {
            this.setDataSource(result['items']);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    applyFilter(filterText: any) {
        this.dataSource.filter = filterText.trim();
    }

    showNewUserDialog(user?: UserEntity): void {
        this.dialog.open(NewUserDialogComponent, {
            width: '600px',
            data: user || false
        }).afterClosed().subscribe(result => {
            if (result === true) {
                this.load();
            }
        });
    }

    edit(user: UserEntity) {
        this.data.setSelectUser(user);
        return this.router.navigateByUrl('user/edit/information');
    }

    delete(index: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(async () => {

                const userId = this.dataSource.data[index]._id;

                try {
                    await this.userService.delete(userId);

                    const ds = this.dataSource.data;
                    ds.splice(index, 1);
                    this.dataSource.data = ds;

                    this.load();
                } catch (err) {
                    this.snackBar.openAutoHide(err.error || err.message);
                }
            });
    }

}
