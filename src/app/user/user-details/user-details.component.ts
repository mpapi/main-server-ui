import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {UserChangesClass} from '../../classes/user-changes.class';
import {UserService} from '../../services/user/user.service';
import {CompanyService} from '../../services/company/company.service';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';

@Component({
    selector: 'mp-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent extends UserChangesClass implements OnInit, OnDestroy {

    @Input() user: UserEntity;
    @Output() userChange: EventEmitter<any> = new EventEmitter<any>();

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected dialog: MatDialog,
                protected router: Router) {
        super(userService, companyService, processService, snackBar, data, router);
    }

    ngOnInit(): void {
        super.onInit();
    }

    ngOnDestroy(): void {
        super.onDestroy();
    }

    addDetailItem() {
        if (!this.user.details) {
            this.user.details = [];
        }

        this.user.details.push({title: null, value: null});
    }
}
