import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {CompanyEntity} from '../../entity/CompanyEntity';
import {UserChangesClass} from '../../classes/user-changes.class';
import {UserService} from '../../services/user/user.service';
import {CompanyService} from '../../services/company/company.service';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';

@Component({
    selector: 'mp-user-information',
    templateUrl: './user-information.component.html',
    styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent extends UserChangesClass implements OnInit, OnDestroy {


    @Input() user: UserEntity;
    @Input() companies: CompanyEntity[];

    @Output() userChange: EventEmitter<any> = new EventEmitter<any>();

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        super(userService, companyService, processService, snackBar, data, router);
    }

    ngOnInit(): void {
        super.onInit();
    }

    ngOnDestroy(): void {
        super.onDestroy();
    }

    isRole(role: string): boolean {
        if (!this.user.roles) {
            this.user.roles = [];
        }

        return this.user.roles.indexOf(role) >= 0;
    }

    toggleRole(role: string) {
        if (!this.user.roles) {
            this.user.roles = [];
        }

        const index: number = _.indexOf(this.user.roles, role);

        if (index !== -1) {
            this.user.roles.splice(index, 1);
        } else {
            this.user.roles.push(role);
        }
    }
}
