import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserDatabaseManagementComponent} from './user-database-management.component';

describe('UserDatabaseManagementComponent', () => {
    let component: UserDatabaseManagementComponent;
    let fixture: ComponentFixture<UserDatabaseManagementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UserDatabaseManagementComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserDatabaseManagementComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
