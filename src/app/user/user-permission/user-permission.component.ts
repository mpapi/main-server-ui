import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {UserChangesClass} from '../../classes/user-changes.class';
import {UserService} from '../../services/user/user.service';
import {CompanyService} from '../../services/company/company.service';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';

@Component({
    selector: 'mp-user-permission',
    templateUrl: './user-permission.component.html',
    styleUrls: ['./user-permission.component.scss']
})
export class UserPermissionComponent extends UserChangesClass implements OnInit, OnDestroy {

    @Input() user: UserEntity;
    @Output() userChange: EventEmitter<any> = new EventEmitter<any>();

    permission = {};

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        super(userService, companyService, processService, snackBar, data, router);

        this.initPermission();
    }

    ngOnInit(): void {
        super.onInit();
    }

    ngOnDestroy(): void {
        super.onDestroy();
    }

    private initPermission() {
        this.user.permission = _.merge({
            users: {
                add: true,
                copy: true,
                edit: true,
                delete: true,
            },

            companies: {
                add: false,
                edit: false,
                delete: false
            },
            mpAnalysis: {
                showTasks: true,
                changePassword: true,
                changeDefaultEndpoint: true
            }
        }, this.user.permission);
    }

}
