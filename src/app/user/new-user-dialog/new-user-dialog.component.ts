import {Component, Inject, OnInit} from '@angular/core';
import {CompanyEntity} from '../../entity/CompanyEntity';
import {UserEntity} from '../../entity/UserEntity';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CompanyService} from '../../services/company/company.service';
import {UserService} from '../../services/user/user.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';

@Component({
    selector: 'mp-new-user-dialog',
    templateUrl: './new-user-dialog.component.html',
    styleUrls: ['./new-user-dialog.component.scss']
})
export class NewUserDialogComponent implements OnInit {


    companies: CompanyEntity[];
    isCopy: boolean;
    user: UserEntity;
    loading = false;

    constructor(public dialogRef: MatDialogRef<NewUserDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public tempUser: any,
                private companyService: CompanyService,
                private userService: UserService,
                private snackBar: SnackBarService) {

        this.isCopy = !!tempUser;
        this.user = tempUser ? {...tempUser} : new UserEntity();
    }

    ngOnInit() {
        this.getCompanies();
    }

    getCompanies() {
        this.companyService.list().then(result => {
            this.companies = result['items'];
        }, err => {
            this.snackBar.openAutoHide(err.error);
        });
    }

    private prepareUser(): UserEntity {
        return {
            email: this.user.email,
            fullname: this.user.fullname,
            username: this.user.username,
            companyId: this.user.companyId,
            password: this.user.password,
            passwordRepeat: this.user.passwordRepeat,
            isAdmin: this.user.isAdmin,
            copyTasks: this.user.copyTasks,
        };
    }

    async save() {

        if (this.loading) {
            return;
        }

        this.loading = true;

        const user = this.prepareUser();

        try {
            this.isCopy
                ? await this.userService.copy({...user, copyUserId: this.tempUser._id})
                : await this.userService.create(user);

            this.snackBar.openAutoHide(user.username + ' kullanıcı adıyla kopyalandı!');

            this.dialogRef.close(true);

        } catch (err) {
            this.snackBar.openAutoHide(err.error || err.message);
        }

        this.loading = false;
    }

}
