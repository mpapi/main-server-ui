import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PaymentRoutingModule} from './payment-routing.module';
import {PaymentComponent} from './payment.component';
import {CoreModule} from '../modules/core.module';

@NgModule({
    declarations: [PaymentComponent],
    imports: [
        CommonModule,
        PaymentRoutingModule,
        CoreModule.forRoot()
    ]
})
export class PaymentModule {
}
