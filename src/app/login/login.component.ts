import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user/user.service';
import {SessionService} from '../services/session/session.service';
import {Router} from '@angular/router';
import {SnackBarService} from '../services/snackBar/snack-bar.service';

@Component({
    selector: 'mp-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loading = false;
    username: string;
    password: string;

    constructor(private userService: UserService,
                private session: SessionService,
                private snackBar: SnackBarService,
                private router: Router) {
    }

    ngOnInit() {

    }

    login() {
        if (this.loading) {
            return;
        }

        this.loading = true;

        this.userService.login(this.username, this.password).then(async result => {
            this.snackBar.openAutoHide('Giriş Başarılı');
            this.session.set(result);
            await this.router.navigateByUrl('/');
        }, error => {
            this.snackBar.openAutoHide(error.error || error.message);
        }).then(() => this.loading = false);
    }

}
