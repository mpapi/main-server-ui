import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxAutoScrollModule} from 'ngx-auto-scroll';
import {MaterialModule} from './material.module';
import {DrawerService} from '../services/drawer/drawer.service';
import {PermPipe} from '../pipes/perm/perm.pipe';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';
import {ApiService} from '../services/api/api.service';
import {SessionService} from '../services/session/session.service';
import {UserService} from '../services/user/user.service';
import {AvatarModule} from 'ng2-avatar';
import {HeaderComponent} from '../components/header/header.component';
import {SnackBarService} from '../services/snackBar/snack-bar.service';
import {AlertComponent} from '../components/alert/alert.component';
import {CategoryService} from '../services/category/category.service';
import {ProcessService} from '../services/process/process.service';
import {MomentModule} from 'ngx-moment';
import {RouterModule} from '@angular/router';
import {RolePipe} from '../pipes/role/role.pipe';

@NgModule({
    declarations: [
        PermPipe,
        RolePipe,
        HeaderComponent,
        AlertComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FlexLayoutModule,
        MaterialModule,
        AvatarModule.forRoot(),
        LoadingBarHttpClientModule,
        LoadingBarRouterModule,
        MomentModule,
        NgxAutoScrollModule
    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FlexLayoutModule,
        MaterialModule,
        AvatarModule,
        LoadingBarHttpClientModule,
        LoadingBarRouterModule,
        HeaderComponent,
        AlertComponent,
        NgxAutoScrollModule,

        PermPipe,
        RolePipe,
    ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                DrawerService,
                ApiService,
                SessionService,
                UserService,
                CategoryService,
                ProcessService,
                SnackBarService
            ]
        };
    }
}
