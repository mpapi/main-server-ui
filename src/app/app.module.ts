import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {MainViewComponent} from './components/main-view/main-view.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from './modules/core.module';
import {DrawerItemComponent} from './components/drawer-item/drawer-item.component';
import {BaseViewComponent} from './components/base-view/base-view.component';

@NgModule({
    declarations: [
        MainViewComponent,
        DrawerItemComponent,
        BaseViewComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        CoreModule.forRoot()
    ],
    providers: [],
    bootstrap: [BaseViewComponent]
})
export class AppModule {
}
