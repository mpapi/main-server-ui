import {Router} from '@angular/router';
import * as _ from 'lodash';
import {UserEntity} from '../entity/UserEntity';
import {CompanyEntity} from '../entity/CompanyEntity';
import {ProcessEntity} from '../entity/ProcessEntity';
import {UserService} from '../services/user/user.service';
import {CompanyService} from '../services/company/company.service';
import {ProcessService} from '../services/process/process.service';
import {SnackBarService} from '../services/snackBar/snack-bar.service';
import {DataService} from '../services/data/data.service';

export class UserChangesClass {

    loading = false;

    tempUser: UserEntity;
    user: UserEntity;

    companies: CompanyEntity[];
    processes: ProcessEntity[];

    constructor(protected userService: UserService,
                protected companyService: CompanyService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {

        this.tempUser = this.data.getSelectUser();
        if (this.tempUser === null) {
            this.router.navigateByUrl('/user/list');
        }

        this.user = _.cloneDeep(this.tempUser);
    }

    onInit() {
        this.load();
    }

    async load() {
        this.loading = true;

        await this.getCompanies();
        await this.getProcesses();

        this.loading = false;
    }

    onDestroy() {
        this.data.setSelectUser(this.user);
    }

    getCompanies() {
        return this.companyService.list().then(result => {
            this.companies = result['items'];
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    getProcesses() {
        return this.processService.list({
            userId: this.user._id,
            queryItems: true,
            hidden: true
        }).then(result => {
            this.processes = result['items'];
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    save() {
        const user = this.prepareUser();

        if (user == null) {
            this.snackBar.openAutoHide('Herhangi Bir Değişiklik Yapılmadı.');
            return;
        }

        if (this.loading) {
            return;
        }
        this.loading = true;

        this.userService.edit(user).then(result => {
            this.snackBar.openAutoHide('Kaydedildi!');
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        }).then(() => this.loading = false);
    }

    private prepareUser() {

        const user: any = {
            userId: this.user['_id']
        };

        Object.keys(this.user).forEach(key => {
            if (!_.isEqual(this.user[key], this.tempUser[key])) {
                user[key] = this.user[key];
            }
        });

        return Object.keys(user).length === 1 ? null : user;
    }

}
