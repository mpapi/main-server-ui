import {Router} from '@angular/router';
import * as _ from 'lodash';
import {UserEntity} from '../entity/UserEntity';
import {ProcessEntity} from '../entity/ProcessEntity';
import {SnackBarService} from '../services/snackBar/snack-bar.service';
import {DataService} from '../services/data/data.service';
import {ProcessService} from '../services/process/process.service';

export class ProcessChangesClass {
    systemTypes: any[] = [
        {
            title: 'SQL',
            value: 'sql'
        },
        {
            title: 'Dosya',
            value: 'file'
        }
    ];

    user: UserEntity;
    processItem: ProcessEntity;
    loadingProcessItem = false;
    tempProcessItem: ProcessEntity;

    constructor(protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        this.user = data.getSelectUser();
        this.setProcessItem(data.getSelectProcess());
    }

    public async save() {
        if (!this.processItem.categoryId) {
            this.snackBar.openAutoHide('Kategori Seçin!');
            return;
        }

        const changes: ProcessEntity = this.data.getChanges<ProcessEntity>(this.tempProcessItem, this.processItem);

        if (Object.keys(changes).length === 0) {
            this.snackBar.openAutoHide('Herhangi bir değişiklik yapmadınız!');
            return;
        }

        try {

            if (this.isEdit()) {
                await this.processService.edit({...changes, processId: this.processItem._id});

                this.data.setSelectProcess(this.processItem);
            } else {
                if (!this.checkValues(changes)) {
                    return;
                }

                const result: any = await this.processService.add(changes);
                this.processItem._id = result.insertedId;

                this.router.navigateByUrl('/process/list');
            }

            this.snackBar.openAutoHide('Kaydedildi!');

        } catch (err) {
            this.snackBar.openAutoHide(err.error || err.message);
        }

    }

    checkValues(changes: ProcessEntity): boolean {
        const errors = [];

        if (_.isEmpty(changes.title)) {
            this.snackBar.openAutoHide('Başlık Girin!');
            return false;
        }

        if (_.isEmpty(changes.processKey)) {
            this.snackBar.openAutoHide('İşlem Anahtarı Girin!');
            return false;
        }

        if (_.isEmpty(changes.dbKey)) {
            this.snackBar.openAutoHide('Veri Erişim Noktasını Seçin!');
            return false;
        }

        if (errors.length > 0) {
            this.snackBar.openAutoHide(errors.join(' - '));
            return false;
        }

        return true;
    }

    protected async load() {
        if (this.loadingProcessItem || !this.isEdit()) {
            return;
        }

        this.loadingProcessItem = true;

        return this.processService.get({
            userId: this.user._id,
            processId: this.processItem._id
        }).then(result => {
            this.setProcessItem(result.item);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        }).then(() => this.loadingProcessItem = false);
    }

    protected setProcessItem(processItem: ProcessEntity) {
        this.tempProcessItem = _.cloneDeep(processItem);
        this.processItem = processItem;
    }

    protected isEdit(): boolean {
        const matchs = this.router.url.match('edit');
        return matchs != null && matchs.length > 0;
    }
}
