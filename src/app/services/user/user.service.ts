import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {UserEntity} from '../../entity/UserEntity';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    path = '/user';

    constructor(private api: ApiService) {
    }

    login(username: string, password: string): Promise<any> {
        return this.api.post(this.path + '/login', {username, password, force: true});
    }

    logout(): Promise<any> {
        return this.api.post(this.path + '/logout');
    }

    create(user: UserEntity) {
        return this.api.post(this.path + '/create', user);
    }

    copy(user: UserEntity) {
        return this.api.post(this.path + '/copy', user);
    }

    list(): Promise<any> {
        return this.api.post(this.path + '/list');
    }

    edit(user: UserEntity) {
        return this.api.post(this.path + '/edit', user);
    }

    delete(userId: string) {
        return this.api.post(this.path + '/delete', {userId});
    }
}
