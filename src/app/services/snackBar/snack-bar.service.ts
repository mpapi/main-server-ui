import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class SnackBarService {

    constructor(private snackBar: MatSnackBar) {
    }

    public open(message: string, actions?: string, config?: MatSnackBarConfig) {
        const snackBarRef = this.snackBar.open(message, actions, config);
        return snackBarRef.onAction();
    }

    public openAutoHide(message: string, actions?: string, config?: MatSnackBarConfig) {
        return this.open(message, actions, {...(config || {}), duration: 3000});
    }
}
