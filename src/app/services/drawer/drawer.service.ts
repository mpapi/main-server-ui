import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NavigationEnd, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class DrawerService {
    public drawer: any;
    public currentUrl = new BehaviorSubject<string>(undefined);

    constructor(private router: Router) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.currentUrl.next(event.urlAfterRedirects);
            }
        });
    }

    public isOverOpened() {
        return this.drawer.mode === 'over' && this.drawer.opened;
    }

    public closeDrawer() {
        this.drawer.close();
    }

    public openDrawer() {
        this.drawer.open();
    }
}
