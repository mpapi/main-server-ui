import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {UserEntity} from '../../entity/UserEntity';
import {CategoryEntity} from '../../entity/CategoryEntity';
import {CompanyEntity} from '../../entity/CompanyEntity';
import {ProcessEntity} from '../../entity/ProcessEntity';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private key = '@AppData';
    private datas: object = {};

    constructor() {
    }

    public set(name, value) {
        this.datas[name] = value;
        this.save();
    }

    protected save() {
        localStorage.setItem(this.key, JSON.stringify(this.datas));
    }

    public get(name, clean?: boolean) {
        this.load();
        const data = _.cloneDeep(this.datas[name]);
        if (clean === true) {
            this.clear(name);
        }
        return data;
    }

    protected load() {
        const datas = localStorage.getItem(this.key);
        this.datas = datas == null ? {} : JSON.parse(datas);
        return this;
    }

    public clear(name: string[]) {
        for (const n of name) {
            delete this.datas[n];
        }
        this.save();
    }

    public setSelectUser(user: UserEntity) {
        return this.set('selectedUser', user);
    }

    public getSelectUser(): UserEntity {
        return this.get('selectedUser');
    }

    public setSelectCategory(category: CategoryEntity) {
        return this.set('selectedCategory', category);
    }

    public getSelectCategory(): CategoryEntity {
        return this.get('selectedCategory');
    }

    public setSelectCompany(company: CompanyEntity) {
        return this.set('selectedCompany', company);
    }

    public getSelectCompany(): UserEntity {
        return this.get('selectedCompany');
    }

    public setSelectProcess(process: ProcessEntity) {
        return this.set('selectedProcess', process);
    }

    public getSelectProcess(): ProcessEntity {
        return this.get('selectedProcess');
    }

    public getChanges<T>(temp: T, item: T): T {
        const changes: any = {};
        Object.keys(item).forEach(key => {
            if (!_.isEqual(item[key], temp[key])) {
                changes[key] = item[key];
            }
        });
        return changes;
    }
}
