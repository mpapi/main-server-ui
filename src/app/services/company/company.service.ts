import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {CompanyEntity} from '../../entity/CompanyEntity';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    path = '/company';

    constructor(private api: ApiService) {
    }

    list(): Promise<any> {
        return this.api.post(this.path + '/list');
    }

    delete(companyId: string) {
        return this.api.post(this.path + '/delete', {companyId});
    }

    edit(company: CompanyEntity) {
        return this.api.post(this.path + '/edit', {...company});
    }

    add(company: CompanyEntity) {
        return this.api.post(this.path + '/add', {...company});
    }
}
