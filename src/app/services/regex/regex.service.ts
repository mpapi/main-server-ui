import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class RegexService {

    constructor() {
    }

    static groups(regExp: RegExp, str: string) {
        const matches = [];
        while (true) {
            const match = regExp.exec(str);
            if (match === null) {
                break;
            }
            // Add capture of group 1 to `matches`
            matches.push(match[1]);
        }
        return matches;
    }
}
