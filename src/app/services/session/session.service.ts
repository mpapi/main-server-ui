import {Injectable} from '@angular/core';
import {UserEntity} from '../../entity/UserEntity';
import {DataService} from '../data/data.service';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class SessionService {

    private user: UserEntity;
    private token: string;

    constructor(private data: DataService) {
    }

    private read() {
        this.user = this.data.get('user');
        this.token = this.data.get('token');
    }

    private save() {
        this.data.set('user', this.user);
        this.data.set('token', this.token);
    }

    public set(result) {
        this.user = result['user'];
        this.token = result['token'];
        this.save();
    }

    public clear() {
        this.data.clear(['user', 'token']);
    }

    public getUser() {
        this.read();
        return this.user;
    }

    public getToken() {
        this.read();
        return this.token;
    }

    public hasToken() {
        this.read();
        return !_.isUndefined(this.token);
    }
}
