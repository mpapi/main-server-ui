import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {SessionService} from '../session/session.service';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    baseUrl = environment.API_URL;

    constructor(protected http: HttpClient,
                protected router: Router,
                protected session: SessionService) {
    }

    public any(method: string, path: string, opts?: any, headerParams?: string[][2]) {

        opts = opts || {};
        opts.responseType = 'json';
        opts.headers = new HttpHeaders();
        opts.headers = opts.headers.set('Content-Type', 'application/json');

        if (this.session.hasToken()) {
            opts.headers = opts.headers.set('token', this.session.getToken());
        }

        if (headerParams != null) {
            for (let i = 0; i < headerParams.length; i++) {
                if (headerParams[i].length === 2) {
                    opts.headers.append(headerParams[i][0], headerParams[i][1]);
                }
            }
        }

        return new Promise((resolve, reject) => {
            this.http.request(method.toUpperCase(), this.baseUrl + path, opts).subscribe(result => {
                if (result['error']) {
                    switch (result['error']) {
                        case 'SESSION_NOT_FOUND':
                        case 'TOKEN_NOT_FOUND':
                            this.session.clear();
                            this.router.navigateByUrl('/login');
                            break;
                    }
                    reject(result);
                } else {
                    resolve(result);
                }
            }, error => {
                reject(error);
            });
        });
    }

    public post(path: string, params?: any, headerParams?: string[][2]) {
        return this.any('post', path, {
            body: params
        }, headerParams);
    }

    public get(path: string, params?: any, headerParams?: string[][2]) {
        return this.any('get', path, {
            params: params
        }, headerParams);
    }

}
