import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';

@Injectable({
    providedIn: 'root'
})
export class ProcessService {

    path = '/process';

    constructor(private api: ApiService) {
    }

    get(params): Promise<any> {
        return this.api.post(this.path + '/get', params);
    }

    list(params): Promise<any> {
        return this.api.post(this.path + '/list', params);
    }

    edit(process): Promise<any> {
        return this.api.post(this.path + '/edit', process);
    }

    add(process): Promise<any> {
        return this.api.post(this.path + '/add', process);
    }

    delete(processId: string) {
        return this.api.post(this.path + '/delete', {processId});
    }

}
