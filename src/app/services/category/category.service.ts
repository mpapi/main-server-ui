import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {CategoryEntity} from '../../entity/CategoryEntity';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    path = '/category';

    constructor(private api: ApiService) {
    }

    add(category: CategoryEntity): Promise<any> {
        return this.api.post(this.path + '/add', {...category});
    }

    edit(category: CategoryEntity): Promise<any> {
        return this.api.post(this.path + '/edit', {...category});
    }

    delete(id: string): Promise<any> {
        return this.api.post(this.path + '/delete', {_id: id});
    }

    list(userId: string, showDeleted: boolean): Promise<any> {
        return this.api.post(this.path + '/list', {userId, showDeleted});
    }
}
