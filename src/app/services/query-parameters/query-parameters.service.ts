import {Injectable} from '@angular/core';
import {QueryEntity} from '../../entity/QueryEntity';
import {UserEntity} from '../../entity/UserEntity';
import * as _ from 'lodash';
import {RegexService} from '../regex/regex.service';

@Injectable({
    providedIn: 'root'
})
export class QueryParametersService {

    getQuery(queryItems: QueryEntity[], queryKey: string): QueryEntity {
        for (const queryItem of queryItems) {
            if (queryItem.queryKey === queryKey) {
                return queryItem;
            }
        }

        return null;
    }

    parametersGenerator(user: UserEntity, queryItem: QueryEntity, includeParams = false): string[] {
        if (!user || !queryItem.command) {
            return [];
        }

        const matchs: string[] | null = RegexService.groups(/\$\$(\w+)/g, queryItem.command);

        const filteredMatchs = _.filter(matchs, (match: string) => {

            const isDefault = !_.find(user.defaults, {name: match});

            // parametreler dahil edilecek veya dahil edilmeyecek.
            const isParams = includeParams || !_.find(queryItem.params, {name: match});

            // user.defaults ve queryItem.params içerisinde bulunmayacakk.
            return isDefault && isParams;
        });

        return _.sortedUniq(filteredMatchs) || [];
    }

}
