import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MpAnalysisRoutingModule} from './mp-analysis-routing.module';
import {CoreModule} from '../modules/core.module';
import {ChartsComponent} from './charts/charts.component';
import {ItemDialogComponent as ChartDialog} from './charts/item-dialog/item-dialog.component';

@NgModule({
    declarations: [
        ChartsComponent,
        ChartDialog
    ],
    imports: [
        CommonModule,
        CoreModule.forRoot(),
        MpAnalysisRoutingModule
    ],
    entryComponents: [
        ChartsComponent, ChartDialog
    ]
})
export class MpAnalysisModule {
}
