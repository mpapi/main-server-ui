import {Component, OnInit} from '@angular/core';
import {ProcessChangesClass} from '../../classes/process-changes.class';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import {QueryParametersService} from '../../services/query-parameters/query-parameters.service';
import {MatDialog} from '@angular/material';
import {MpChartEntity} from '../../entity/MpChartEntity';
import * as _ from 'lodash';
import {ItemDialogComponent} from './item-dialog/item-dialog.component';

@Component({
    selector: 'mp-charts',
    templateUrl: './charts.component.html',
    styleUrls: ['./charts.component.scss']
})
export class ChartsComponent extends ProcessChangesClass implements OnInit {

    charts: MpChartEntity[];

    constructor(protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected queryParams: QueryParametersService,
                protected dialog: MatDialog,
                protected router: Router) {
        super(processService, snackBar, data, router);
        this.setMpAnalysis();
    }

    ngOnInit() {
        this.load().then(() => this.setMpAnalysis());
    }

    setMpAnalysis() {

        if (!_.isArray(this.processItem.charts)) {
            this.processItem.charts = [];
        }

        this.charts = this.processItem.charts;
    }

    show(index?: number) {

        const queryParameter = this.queryParams.getQuery(this.processItem.queryItems, 'main');

        if (queryParameter === null) {
            this.snackBar.openAutoHide('Sorgu Listesinde `main` Sorgusu Bulunamadı.');
            return;
        }

        const paramDialog = this.dialog.open(ItemDialogComponent, {
            minWidth: '600px',
            data: {
                chartItem: _.cloneDeep(this.charts[index])
            }
        });
        paramDialog.afterClosed().subscribe(result => {

            if (result) {
                if (_.isUndefined(index)) {
                    this.charts.push(result);
                } else {
                    this.charts[index] = result;
                }
            }
        });
    }

    delete(index: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(() => {
                this.charts.splice(index, 1);
            });
    }

    setDefault(index: number) {
        for (let i = 0; i < this.charts.length; i++) {
            this.charts[i].default = i === index;
        }
    }

}
