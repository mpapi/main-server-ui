import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {QueryParametersService} from '../../../services/query-parameters/query-parameters.service';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import * as uuid from 'uuid';
import * as _ from 'lodash';

@Component({
    selector: 'mp-item-dialog',
    templateUrl: './item-dialog.component.html',
    styleUrls: ['./item-dialog.component.scss']
})
export class ItemDialogComponent implements OnInit {

    chartItem: MpChartEntity;

    constructor(public dialogRef: MatDialogRef<ItemDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public datas: {
                    chartItem: MpChartEntity
                },
                private queryParameters: QueryParametersService) {

        this.chartItem = datas.chartItem || new MpChartEntity();
        this.chartItem.key = this.chartItem.key || uuid.v1().substring(0, 8);
    }

    ngOnInit(): void {
    }

    addChart() {
        if (!_.isArray(this.chartItem.series)) {
            this.chartItem.series = [];
        }
        this.chartItem.series.push({});
    }

    removeConfig(index: number) {
        this.chartItem.series.splice(index, 1);
    }
}
