import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DisplayGrid, GridsterConfig, GridsterItem, GridType} from 'angular-gridster2';
import * as _ from 'lodash';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {MpDashboardEntity} from '../../entity/MpDashboardEntity';
import {ProcessEntity} from '../../entity/ProcessEntity';
import {UserEntity} from '../../entity/UserEntity';
import {MpPresentParamEntity} from '../../entity/MpPresentParamEntity';
import {UserService} from '../../services/user/user.service';

@Component({
    selector: 'mp-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    loading = false;
    user: UserEntity;

    options: GridsterConfig = {
        mobileBreakpoint: 0,
        minCols: 2,
        maxCols: 2,
        gridType: GridType.VerticalFixed,
        fixedColWidth: 144,
        fixedRowHeight: 144,
        displayGrid: DisplayGrid.Always,
        resizable: {
            // delayStart: 200,
            enabled: true,
            handles: {
                s: false,
                e: false,
                n: false,
                w: false,
                se: true,
                ne: false,
                sw: false,
                nw: false
            }
        },
        draggable: {
            // delayStart: 300,
            enabled: true,
            ignoreContent: true
        }
    };

    userDashboard: MpDashboardEntity[];
    dashboard: Array<GridsterItem> = [];

    processList: ProcessEntity[];

    constructor(protected userService: UserService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        this.user = data.getSelectUser();
    }

    ngOnInit() {
        this.userDashboard = (this.user && this.user.dashboard) || [];
        this.load();
    }

    load() {
        this.processService.list({
            userId: this.user._id,
            mpAnalysis: true
        }).then(result => {

            this.processList = result['items'];

            this.toDashboard();
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }


    addItem() {
        this.dashboard.push({
            x: -1,
            y: -1,
            rows: 1,
            cols: 1,
            propName: {
                processKey: null,
                presentParamKey: null,
                chartKey: null
            }
        });
    }

    private toDashboard() {
        const dashboard = [];

        for (const item of this.userDashboard) {
            dashboard.push({
                cols: item.cols,
                rows: item.rows,
                x: item.x,
                y: item.y,
                propName: {
                    processKey: item.processKey,
                    presentParamKey: item.presentParamKey,
                    chartKey: item.chartKey
                }
            });
        }

        this.dashboard = dashboard;
    }

    getPresentParams(processKey: string): MpPresentParamEntity[] {
        for (const processItem of this.processList) {
            if (processItem.processKey === processKey) {
                return processItem.presentParams;
            }
        }

        return [];
    }

    getCharts(processKey: string): MpPresentParamEntity[] {
        for (const processItem of this.processList) {
            if (processItem.processKey === processKey) {
                return processItem.charts;
            }
        }

        return [];
    }

    private toModel(): MpDashboardEntity[] {
        const dashboard: MpDashboardEntity[] = [];
        for (const item of this.dashboard) {
            dashboard.push({
                cols: item.cols,
                rows: item.rows,
                x: item.x,
                y: item.y,
                processKey: item.propName.processKey,
                presentParamKey: item.propName.presentParamKey,
                chartKey: item.propName.chartKey
            });
        }

        return dashboard;
    }

    save() {

        const dashboard = this.toModel();

        for (const item of dashboard) {

            if (_.isUndefined(item.processKey) || _.isUndefined(item.presentParamKey)) {
                this.snackBar.openAutoHide('Eksik Seçim yapılmış veri mevcut.');
                return;
            }
        }

        if (this.loading) {
            return;
        }
        this.loading = true;

        const values: any = {
            userId: this.user._id,
            dashboard
        };

        this.userService.edit(values).then(result => {
            this.snackBar.openAutoHide('Kaydedildi!');
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        }).then(() => this.loading = false);
    }

}
