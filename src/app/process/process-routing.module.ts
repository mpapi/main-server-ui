import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {ListComponent} from './list/list.component';
import {InformationComponent} from './information/information.component';
import {QueriesComponent} from './queries/queries.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PresentParamsComponent} from './present-params/present-params.component';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'users'},
    {path: 'users', component: UsersComponent},
    {path: 'list', component: ListComponent},
    {path: 'mp-dashboard', component: DashboardComponent},
    {path: 'add/information', component: InformationComponent},
    {path: 'edit/information', component: InformationComponent},
    {path: 'edit/queries', component: QueriesComponent},
    {path: 'edit/present-params', component: PresentParamsComponent},
    {
        path: 'edit/mp-analysis',
        loadChildren: '../mp-analysis/mp-analysis.module#MpAnalysisModule'
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProcessRoutingModule {
}
