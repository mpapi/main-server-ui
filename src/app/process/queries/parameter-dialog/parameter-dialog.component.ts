import {Component, Inject, OnInit} from '@angular/core';
import {QueryParamEntity} from '../../../entity/QueryParamEntity';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {QueryEntity} from '../../../entity/QueryEntity';
import * as _ from 'lodash';
import {SnackBarService} from '../../../services/snackBar/snack-bar.service';

@Component({
    selector: 'mp-parameter-dialog',
    templateUrl: './parameter-dialog.component.html',
    styleUrls: ['./parameter-dialog.component.scss']
})
export class ParameterDialogComponent implements OnInit {

    loading = false;
    parameterTypes: any[] = [
        {
            title: 'Sorgu Listesi',
            value: 'process'
        },
        {
            title: 'Sıralı İşlem',
            value: 'successive'
        },
        {
            title: 'Metin Girişi',
            value: 'input'
        },
        {
            title: 'Tarih Girişi',
            value: 'date'
        },
        {
            title: 'Saat Girişi',
            value: 'time'
        },
        {
            title: 'Tarih-Saat Girişi',
            value: 'datetime'
        },
        {
            title: 'Dropdown List',
            value: 'select'
        }
    ];
    param: QueryParamEntity;
    queryItems: QueryEntity[] = [];

    constructor(public snackBar: SnackBarService,
                public dialogRef: MatDialogRef<ParameterDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public datas: {
                    matchs: string[],
                    param: QueryParamEntity
                    queryItems: QueryEntity[]
                }) {

        this.param = datas.param || new QueryParamEntity();
        this.queryItems = datas.queryItems;
    }

    ngOnInit() {
    }

    save() {
        if (_.isEmpty(this.param.name)) {
            return this.snackBar.openAutoHide('Parametre adını seçin.');
        }

        if (_.isEmpty(this.param.type)) {
            return this.snackBar.openAutoHide('Tipini seçin.');
        }

        this.dialogRef.close(this.param);
    }
}
