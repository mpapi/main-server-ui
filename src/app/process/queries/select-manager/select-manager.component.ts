import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QuerySelectEntity} from '../../../entity/QuerySelectEntity';

@Component({
    selector: 'mp-select-manager',
    templateUrl: './select-manager.component.html',
    styleUrls: ['./select-manager.component.scss']
})
export class SelectManagerComponent implements OnInit {

    @Input() select: QuerySelectEntity;
    @Output() selectChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit() {
    }

    clear() {
        this.select.label = null;
        this.select.textColumn = null;
        this.select.valueColumn = null;
    }
}
