import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {QueryParamEntity} from '../../../entity/QueryParamEntity';
import {ParameterDialogComponent} from '../parameter-dialog/parameter-dialog.component';
import {SnackBarService} from '../../../services/snackBar/snack-bar.service';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import * as _ from 'lodash';
import {QueryParametersService} from '../../../services/query-parameters/query-parameters.service';
import {QueryEntity} from '../../../entity/QueryEntity';
import {UserEntity} from '../../../entity/UserEntity';

@Component({
    selector: 'mp-parameters-manager',
    templateUrl: './parameters-manager.component.html',
    styleUrls: ['./parameters-manager.component.scss']
})
export class ParametersManagerComponent implements OnInit {

    @Input() process: ProcessEntity;
    @Input() user: UserEntity;
    @Input() queryItem: QueryEntity;
    @Output() queryItemChange: EventEmitter<any> = new EventEmitter<any>();
    matchs: string[];

    constructor(public snackBar: SnackBarService,
                private queryParameters: QueryParametersService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
    }

    getIndex(match) {
        for (let i = 0; i < (this.queryItem.params || []).length; i++) {
            if (this.queryItem.params[i].name === match) {
                return i;
            }
        }
        return undefined;
    }

    show(index?: number) {
        this.matchs = this.queryParameters.parametersGenerator(this.user, this.queryItem);

        const paramDialog = this.dialog.open(ParameterDialogComponent, {
            minWidth: '500px',
            data: {
                matchs: this.matchs,
                param: _.cloneDeep(this.queryItem.params[index]),
                queryItems: this.process.queryItems
            }
        });
        paramDialog.afterClosed().subscribe((result: QueryParamEntity) => {

            if (result) {
                if (_.isUndefined(index)) {
                    this.queryItem.params.push(result);
                } else {
                    this.queryItem.params[index] = result;
                }
            }
        });
    }

    delete(index: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(() => {
                this.queryItem.params.splice(index, 1);
            });
    }
}
