import {Component, OnInit} from '@angular/core';
import {ProcessChangesClass} from '../../classes/process-changes.class';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';

@Component({
    selector: 'mp-queries',
    templateUrl: './queries.component.html',
    styleUrls: ['./queries.component.scss']
})
export class QueriesComponent extends ProcessChangesClass implements OnInit {

    loading = false;

    constructor(protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        super(processService, snackBar, data, router);
    }

    async ngOnInit() {
        await this.load();
        this.changeTabs();
    }

    changeTabs() {
        this.loading = false;
        setTimeout(() => this.loading = true, 600);
    }

    addQueryItem() {

        if (!_.isArray(this.processItem.queryItems)) {
            this.processItem.queryItems = [];
        }

        const length = (this.processItem.queryItems.length + 1);
        this.processItem.queryItems.push({
            title: length + '. Process',
            queryKey: length === 1 ? 'main' : null,
            command: null,
            params: [],
            select: {},
            table: [],
            loopQuery: true
        });
    }
}
