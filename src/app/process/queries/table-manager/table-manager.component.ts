import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QueryTableRowEntity} from '../../../entity/QueryTableRowEntity';

@Component({
    selector: 'mp-table-manager',
    templateUrl: './table-manager.component.html',
    styleUrls: ['./table-manager.component.scss']
})
export class TableManagerComponent implements OnInit {

    @Input() table: QueryTableRowEntity[];
    @Output() tableChange: EventEmitter<any> = new EventEmitter<any>();

    aligns: any[] = [
        {
            title: 'Sola Yasla',
            value: 'left'
        },
        {
            title: 'Ortala',
            value: 'center'
        },
        {
            title: 'Sağ Yasla',
            value: 'right'
        }
    ];

    constructor() {
    }

    ngOnInit() {
    }
}
