import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MpPresentParamEntity} from '../../../entity/MpPresentParamEntity';
import {QueryParametersService} from '../../../services/query-parameters/query-parameters.service';
import * as uuid from 'uuid';
import {QueryEntity} from '../../../entity/QueryEntity';
import {UserEntity} from '../../../entity/UserEntity';

@Component({
    selector: 'mp-item-dialog',
    templateUrl: './item-dialog.component.html',
    styleUrls: ['./item-dialog.component.scss']
})
export class ItemDialogComponent implements OnInit {

    presentItem: MpPresentParamEntity;
    matchs: string[] = [];

    constructor(public dialogRef: MatDialogRef<ItemDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public datas: {
                    presentItem: MpPresentParamEntity,
                    user: UserEntity,
                    queryItem: QueryEntity
                },
                private queryParameters: QueryParametersService) {

        this.presentItem = datas.presentItem || new MpPresentParamEntity();
        this.presentItem.params = this.presentItem.params || {};
        this.presentItem.key = this.presentItem.key || uuid.v1().substring(0, 8);
    }

    ngOnInit() {
        this.generateMatchs();
    }

    generateMatchs() {
        this.matchs = this.queryParameters.parametersGenerator(this.datas.user, this.datas.queryItem, true);
    }

    removeParam(index: number) {
        const params = this.matchs.splice(index, 1);
        if (params.length > 0) {
            delete this.presentItem.params[params[0]];
        }
    }
}
