import {Component, OnInit} from '@angular/core';
import {ProcessChangesClass} from '../../classes/process-changes.class';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {MatDialog} from '@angular/material';
import {ItemDialogComponent} from './item-dialog/item-dialog.component';
import {QueryParametersService} from '../../services/query-parameters/query-parameters.service';
import {MpPresentParamEntity} from '../../entity/MpPresentParamEntity';

@Component({
    selector: 'mp-present-params',
    templateUrl: './present-params.component.html',
    styleUrls: ['./present-params.component.scss']
})
export class PresentParamsComponent extends ProcessChangesClass implements OnInit {

    presentParams: MpPresentParamEntity[];

    constructor(protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected queryParams: QueryParametersService,
                protected dialog: MatDialog,
                protected router: Router) {
        super(processService, snackBar, data, router);
        this.setMpAnalysis();
    }

    ngOnInit() {
        this.load().then(() => this.setMpAnalysis());
    }

    setMpAnalysis() {

        if (!_.isArray(this.processItem.presentParams)) {
            this.processItem.presentParams = [];
        }

        this.presentParams = this.processItem.presentParams;
    }

    show(index?: number) {

        const queryItem = this.queryParams.getQuery(this.processItem.queryItems, 'main');

        if (queryItem === null) {
            this.snackBar.openAutoHide('Sorgu Listesinde `main` Sorgusu Bulunamadı.');
            return;
        }

        const paramDialog = this.dialog.open(ItemDialogComponent, {
            minWidth: '600px',
            data: {
                presentItem: _.cloneDeep(this.presentParams[index]),
                user: this.user,
                queryItem
            }
        });
        paramDialog.afterClosed().subscribe(result => {
            if (result) {
                if (_.isUndefined(index)) {
                    this.presentParams.push(result);
                } else {
                    this.presentParams[index] = result;
                }
            }
        });
    }

    delete(index: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(() => {
                this.presentParams.splice(index, 1);
            });
    }

    setDefault(index: number) {
        for (let i = 0; i < this.presentParams.length; i++) {
            this.presentParams[i].default = i === index;
        }
    }
}
