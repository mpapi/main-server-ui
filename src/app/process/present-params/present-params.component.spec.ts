import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PresentParamsComponent} from './present-params.component';

describe('PresentParamsComponent', () => {
    let component: PresentParamsComponent;
    let fixture: ComponentFixture<PresentParamsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PresentParamsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PresentParamsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
