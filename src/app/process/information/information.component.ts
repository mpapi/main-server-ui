import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProcessService} from '../../services/process/process.service';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {ProcessEntity} from '../../entity/ProcessEntity';
import {CategoryService} from '../../services/category/category.service';
import {CategoryEntity} from '../../entity/CategoryEntity';
import {FormControl} from '@angular/forms';
import * as _ from 'lodash';
import {Router} from '@angular/router';
import {ProcessChangesClass} from '../../classes/process-changes.class';

@Component({
    selector: 'mp-information',
    templateUrl: './information.component.html',
    styleUrls: ['./information.component.scss']
})
export class InformationComponent extends ProcessChangesClass implements OnInit, OnDestroy {

    flatCategories: CategoryEntity[] = [];
    loading = false;

    categoryControl: FormControl = new FormControl();

    constructor(protected categoryService: CategoryService,
                protected processService: ProcessService,
                protected snackBar: SnackBarService,
                protected data: DataService,
                protected router: Router) {
        super(processService, snackBar, data, router);
    }

    ngOnInit() {
        this.loadCategories();
        this.load();
    }

    ngOnDestroy() {
        this.data.setSelectProcess(this.processItem);
    }

    setProcessItem(processItem: ProcessEntity) {
        this.tempProcessItem = _.cloneDeep(processItem);
        this.processItem = processItem;
    }

    loadCategories() {
        this.categoryService.list(this.user._id, false).then(result => {
            this.flatCategoriesMapper(result.items, 1);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    flatCategoriesMapper(items: CategoryEntity[], depth: number) {
        for (let i = 0; i < items.length; i++) {
            if (items[i]._id === this.processItem.categoryId) {
                // this.processItem.categoryTitle = items[i].title;
                this.categoryControl.setValue(items[i].title);
            }
            this.flatCategories.push({...items[i], depth});

            if (items[i].children.length > 0) {
                this.flatCategoriesMapper(items[i].children, depth + 1);
            }
        }
    }

    categorySelect(category: CategoryEntity) {
        this.categoryControl.setValue(category.title);
        this.processItem.categoryId = category._id;
        this.processItem.categoryTitle = category.title;
    }


}
