import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SnackBarService} from '../../services/snackBar/snack-bar.service';
import {DataService} from '../../services/data/data.service';
import {Router} from '@angular/router';
import {UserEntity} from '../../entity/UserEntity';
import {ProcessService} from '../../services/process/process.service';
import {ProcessEntity} from '../../entity/ProcessEntity';

@Component({
    selector: 'mp-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

    displayedColumns: string[] = ['title', 'category', 'processKey', 'dbKey', 'hidden', 'actions'];
    dataSource = new MatTableDataSource<any>([]);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    user: UserEntity;

    constructor(private processService: ProcessService,
                private snackBar: SnackBarService,
                private data: DataService,
                public dialog: MatDialog,
                private router: Router) {
        this.user = this.data.getSelectUser();
    }

    ngOnInit() {
        const ds = this.data.get('processList');
        if (ds != null) {
            this.setDataSource(ds);
        }

        this.load();
    }

    ngOnDestroy() {
        this.data.set('processList', this.dataSource.data);
    }

    setDataSource(data: any[]) {
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    load() {
        this.processService.list({
            userId: this.user._id,
            hidden: true
        }).then(result => {
            this.setDataSource(result['items']);
        }, err => {
            this.snackBar.openAutoHide(err.error || err.message);
        });
    }

    applyFilter(filterText: any) {
        this.dataSource.filter = filterText.trim();
    }

    gotoEdit(process: ProcessEntity) {
        this.data.setSelectProcess(process);
        return this.router.navigateByUrl('/process/edit/information');
    }

    gotoAdd() {
        this.data.setSelectProcess({
            hidden: false,
            queryItems: [],
            events: [],
            presentParams: [],
            charts: []
        });
        return this.router.navigateByUrl('/process/add/information');
    }

    delete(index?: number) {
        this.snackBar
            .openAutoHide('Silinsin mi?', 'Evet')
            .subscribe(() => {
                const companyId = this.dataSource.data[index]._id;

                const ds = this.dataSource.data;
                ds.splice(index, 1);
                this.dataSource.data = ds;

                this.processService.delete(companyId)
                    .then(() => this.load(), err => {
                        this.load();
                        this.snackBar.openAutoHide(err.error || err.message);
                    });
            });
    }
}
