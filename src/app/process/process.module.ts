import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProcessRoutingModule} from './process-routing.module';
import {UsersComponent} from './users/users.component';
import {ListComponent} from './list/list.component';
import {CoreModule} from '../modules/core.module';
import {InformationComponent} from './information/information.component';
import {QueriesComponent} from './queries/queries.component';
import {ParametersManagerComponent} from './queries/parameters-manager/parameters-manager.component';
import {TableManagerComponent} from './queries/table-manager/table-manager.component';
import {SelectManagerComponent} from './queries/select-manager/select-manager.component';
import {ParameterDialogComponent} from './queries/parameter-dialog/parameter-dialog.component';
import {CodemirrorModule} from '@ctrl/ngx-codemirror';

import 'codemirror/mode/sql/sql';
import {GridsterModule} from 'angular-gridster2';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PresentParamsComponent} from './present-params/present-params.component';
import {ItemDialogComponent as PresentDialog} from './present-params/item-dialog/item-dialog.component';

@NgModule({
    declarations: [
        UsersComponent,
        ListComponent,
        DashboardComponent,
        InformationComponent,
        QueriesComponent,
        TableManagerComponent,
        SelectManagerComponent,
        ParametersManagerComponent,
        ParameterDialogComponent,
        PresentParamsComponent,
        PresentDialog,
    ],
    imports: [
        CommonModule,
        CoreModule.forRoot(),
        CodemirrorModule,
        GridsterModule,
        ProcessRoutingModule
    ],
    entryComponents: [
        QueriesComponent, ParameterDialogComponent,
        PresentParamsComponent, PresentDialog
    ]
})
export class ProcessModule {
}
