import {Pipe, PipeTransform} from '@angular/core';
import {SessionService} from '../../services/session/session.service';
import * as _ from 'lodash';

@Pipe({
    name: 'role'
})
export class RolePipe implements PipeTransform {

    constructor(protected session: SessionService) {
    }

    transform(user: any, role: any): boolean {
        user = user || this.session.getUser();

        return _.indexOf(user.roles, role) >= 0;
    }
}
