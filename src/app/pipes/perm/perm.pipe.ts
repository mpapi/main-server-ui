import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';
import {SessionService} from '../../services/session/session.service';

@Pipe({
    name: 'perm'
})
export class PermPipe implements PipeTransform {

    constructor(protected session: SessionService) {
    }

    transform(user: any, path: string): any {
        user = user || this.session.getUser();
        const perm = user && user.permission;
        return _.result(perm, path);
    }

}
