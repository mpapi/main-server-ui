export class RequestEntity {

    processKey?: string;
    defaults?: any;
    parameters?: {
        queryKey?: string,
        values?: any
    }[];

}
