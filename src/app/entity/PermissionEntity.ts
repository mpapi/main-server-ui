export class PermissionEntity {

    users: {
        add: boolean,
        copy: boolean,
        edit: boolean,
        delete: boolean,
    };

    companies: {
        add: boolean,
        edit: boolean,
        delete: boolean
    };

    mpAnalysis!: {
        showTasks: boolean,
        changePassword: boolean,
        changeDefaultEndpoint: boolean
    };

}
