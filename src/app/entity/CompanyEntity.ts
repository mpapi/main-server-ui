export class CompanyEntity {
    _id?: string;
    title?: string;
    machineKey?: string;
    createdTime?: string;
    expiredTime?: string;
    userLimit?: number;
    verified?: boolean;
    active?: boolean;
    added?: {
        userId?: string,
        datetime?: string;
    };
    deleted?: boolean;
}
