import {RequestEntity} from './RequestEntity';

export class TaskEntity<T> {

    key?: string;
    title?: string;
    module?: string;
    active?: boolean;
    data?: RequestEntity[];

    options?: T;
}
