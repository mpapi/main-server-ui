import {ServerEntity} from './ServerEntity';
import {DbEntity} from './DbEntity';
import {PermissionEntity} from './PermissionEntity';
import {DefaultEntity} from './DefaultEntity';
import {ActionEntity} from './ActionEntity';
import {CompanyEntity} from './CompanyEntity';
import {UserDetailEntity} from './UserDetailEntity';
import {MpDashboardEntity} from './MpDashboardEntity';
import {TaskEntity} from './TaskEntity';

export class UserEntity {
    _id?: string;
    companyId?: string;
    company?: CompanyEntity;
    fullname?: string;
    email?: string;
    username?: string;
    password?: string;
    details?: UserDetailEntity[];
    expiredTime?: string;
    verified?: boolean;
    active?: boolean;
    added?: ActionEntity;
    adminCompanies?: string[];
    servers?: ServerEntity[];
    dbList?: DbEntity[];
    tasks?: TaskEntity<any>[];
    defaults?: DefaultEntity[];
    sessions?: any[];
    roles?: string[];
    permission?: PermissionEntity;
    deleted?: ActionEntity | false;

    copyUserId?: string;
    passwordRepeat?: string;
    dashboard?: MpDashboardEntity[];

    // only
    isAdmin!: boolean;
    copyTasks!: boolean;
}
