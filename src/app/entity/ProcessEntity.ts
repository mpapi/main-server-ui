import {ProcessEventEntity} from './ProcessEventEntity';
import {QueryEntity} from './QueryEntity';
import {MpPresentParamEntity} from './MpPresentParamEntity';
import {MpChartEntity} from './MpChartEntity';

export class ProcessEntity {
    _id?: string;
    categoryId?: string;
    categoryTitle?: string;
    title?: string;
    processKey?: string;
    systemType?: string;
    expiredTime?: string;
    hidden?: boolean;
    dbKey?: string;
    icon?: string;
    queryItems?: QueryEntity[];
    events?: ProcessEventEntity[];

    presentParams?: MpPresentParamEntity[];
    charts?: MpChartEntity[];
}
