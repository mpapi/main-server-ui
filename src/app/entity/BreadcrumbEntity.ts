export class BreadcrumbEntity {
    displayName: string;
    iconName?: string;
    route?: string;
}
