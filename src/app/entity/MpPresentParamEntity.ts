import {KeyValueEntity} from './KeyValueEntity';

export class MpPresentParamEntity {
    key?: string;
    title?: string;
    subtitle?: string;
    default?: boolean;
    icon?: string;
    description?: string;
    params?: KeyValueEntity<string, any>;
}
