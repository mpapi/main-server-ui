export interface NavEntity {
    displayName: string;
    disabled?: boolean;
    iconName?: string;
    route?: string;
    expandToParent?: boolean;
    showWhenParents?: string[];
    children?: NavEntity[];
    externalUrl?: boolean;
    divider?: {
        after?: boolean,
        before?: boolean
    };
}
