export class ServerEntity {
    title: string;
    host: string;
    port: number;
}
