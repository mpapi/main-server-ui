export class ProcessEventEntity {
    title?: string;
    default?: boolean;
    icon?: string;
    queryKey?: string;
    params?: any;
}
