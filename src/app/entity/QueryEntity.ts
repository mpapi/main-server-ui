import {QueryParamEntity} from './QueryParamEntity';
import {ProcessEventEntity} from './ProcessEventEntity';
import {QuerySelectEntity} from './QuerySelectEntity';
import {QueryTableRowEntity} from './QueryTableRowEntity';

export class QueryEntity {
    title?: string;
    queryKey?: string;
    command?: string;
    params?: QueryParamEntity[];
    select?: QuerySelectEntity;
    table?: QueryTableRowEntity[];
    loopQuery?: boolean;
    events?: ProcessEventEntity[];
}
