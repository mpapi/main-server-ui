export class CategoryEntity {
    _id?: string;
    userId?: string;
    parentId?: string;
    title?: string;
    icon?: string;
    order?: number;
    deleted?: boolean;
    processCount?: number;
    depth?: number;
    children?: CategoryEntity[];
}
